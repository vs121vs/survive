import enum
import random
from re import L
from turtle import width
import numpy as np

from world_builder import world_builder
from game.objects import *
from game.agent_ai import PLAYER_AIS
from game.game import Game
from game.const import *

def create_combat_tutorial_game(
    player_ai=None,
    map_size=(20, 20),
    init_location=(10, 10)
):
    """
    Tutorial map with combat-oriented win condition.
    @Author: Wey Yeh

    Args:
        player_one_ai: Agent instance to use as the player AI
        map_size: Size of the tutorial map
        init_location: Initial location of the player AI
    """
    wb = world_builder(map_size[0], map_size[1], True)

    # Initialize grid locations for monsters and items
    grid_centers = [
        (5, 5),
        (5, 15),
        (15, 5),
        (15, 15)
    ]
    np.random.shuffle(grid_centers)
    monster_grid_locations, item_grid_locations = grid_centers[:2], grid_centers[2:]

    # Initialize monsters and items
    for location in monster_grid_locations:
        random_shift = np.random.randint(-1, 1, size=2)
        location = (
            location[0] + random_shift[0],
            location[1] + random_shift[1]
        )
        wb.add_grid(
            grid_center=location,
            grid_size=(4, 4),
            grid_config={
                'items' : 1,
                'monsters' : 1
            },
            grid_types={
                'monsters' : [MonsterM]
            }
        )
    
    fixed_items = [Sword, Water]
    for i, location in enumerate(item_grid_locations):
        random_shift = np.random.randint(-1, 1, size=2)
        location = (
            location[0] + random_shift[0],
            location[1] + random_shift[1]
        )
        wb.add_grid(
            grid_center=location,
            grid_size=(4, 4),
            grid_config={
                'items' : 1,
                'terrain' : 2
            },
            # Make sure to have at least one weapon and food
            grid_types={
                'items' : [fixed_items[i]]
            }
        )

    strawberry_locations = [
        (5, 10),
        (10, 5),
        (15, 10),
        (10, 15)
    ]
    # NOTE: Using water to make tutorial easier
    wb.add_items(
        [Strawberry() for _ in range(len(strawberry_locations))],
        strawberry_locations
    )

    # Initialize player AI
    player = Player(
        player_id=1, agent_ai=player_ai or PLAYER_AIS[0], 
        timeout=None)

    wb.add_players(players=[player], locs=[init_location])
    game = Game(
        world=wb.get_world(), 
        players=wb.get_players(), 
        max_rounds=150, 
        win_condition='no_monsters_left')

    return game

def create_collection_tutorial_game(
    player_ai=None
):
    """
    Simple tutorial map with single item with win condition of 
    collecting and using all items
    @Author: Wey Yeh
    """
    wb = world_builder(
        length=6, 
        width=10, 
        add_border=True)

    # Initialize single strawberry
    wb.add_items([Strawberry()], locs=[(4, 8)])

    # Initialize player AI
    player = Player(
        player_id=1, 
        agent_ai=player_ai or PLAYER_AIS[0], 
        timeout=None)

    wb.add_players(players=[player], locs=[(2, 2)])

    game = Game(
        world=wb.get_world(), 
        players=wb.get_players(), 
        max_rounds=100, 
        win_condition='no_items_left')

    return game

def create_arena_map(
    player_ais=[],
    unit_types=[],
    player_locs=[
        (25, 15),
        (5, 15),
        (18, 8),
        (18, 22),
        (12, 8),
        (12, 22)
    ],
    max_rounds=100
):
    """
    Arena map configuration for 6 players

    Args:
        player_ais(list): List of classes for each player AI. If empty list is passed, random AIs will be used by default

    """
    # Initialize random player AIs from select list
    if not player_ais:
        player_ais = [SimpleAI for _ in range(6)]
    if not unit_types:
        unit_types = [MELEE for _ in range(6)]

    wb = world_builder(
        length=32, 
        width=32, 
        add_border=True)

    ###########
    # Players #
    ###########
    players = []
    for i, ai in enumerate(player_ais):
        players.append(
            Player(
                player_id=i+1,
                agent_ai=ai
            )
        )
        # Direct instantiation since it is abstracted out of constructor 
        players[i].unit_type = unit_types[i]
        players[i].agent_ai.unit_type = unit_types[i]
    
    np.random.shuffle(players) # Randomly assign locations
    wb.add_players(players=players, locs=player_locs)

    ################
    # Strawberries #
    ################
    # Bottom left
    bottom_left_strawberry_locs = [
        (21, 4),
        (21, 12)
    ]
    wb.add_items(
        [Strawberry() for i in range(len(bottom_left_strawberry_locs))],
        bottom_left_strawberry_locs
    )

    # Middle left
    middle_left_strawberry_locs = [
        (15, 4)
    ]
    wb.add_items(
        [Strawberry() for i in range(len(middle_left_strawberry_locs))],
        middle_left_strawberry_locs
    )

    # Top left
    top_left_strawberry_locs = [
        (9, 4),
        (9, 12),
        (2, 12)
    ]
    wb.add_items(
        [Strawberry() for i in range(len(top_left_strawberry_locs))],
        top_left_strawberry_locs
    )

    # Top right
    top_right_strawberry_locs = [
        (9, 26),
        (9, 18)
    ]
    wb.add_items(
        [Strawberry() for i in range(len(top_right_strawberry_locs))],
        top_right_strawberry_locs
    )

    # Middle right
    middle_right_strawberry_locs = [
        (15, 26)
    ]
    wb.add_items(
        [Strawberry() for i in range(len(middle_right_strawberry_locs))],
        middle_right_strawberry_locs
    )

    # Bottom right
    bottom_right_strawberry_locs = [
        (21, 26),
        (21, 18),
        (28, 18)
    ]
    wb.add_items(
        [Strawberry() for i in range(len(bottom_right_strawberry_locs))],
        bottom_right_strawberry_locs
    )

    ###########
    # Weapons #
    ###########
    sword_locs = [
        (28, 14),
        (2, 16),
        (12, 4),
        (18, 4),
        (12, 26),
        (18, 26)
    ]
    wb.add_items(
        [Sword() for i in range(len(sword_locs))],
        sword_locs
    )

    ############
    # Monsters #
    ############
    grid_center_locs = [
        (25, 8),
        (5, 8),
        (25, 22),
        (5, 22)
    ]
    grid_items = [
        Bow, Shoes, Armor, Bow
    ]
    np.random.shuffle(grid_items)
    for i, center in enumerate(grid_center_locs):
        wb.add_grid(
            center,
            grid_size=(4, 4),
            grid_config={
                'items' : 2,
                'monsters' : 1,
                'terrain' : 2
            },
            grid_types={
                'items' : [grid_items[i], Water],
                'monsters' : [MonsterM]
            }
        )
    
    # Boss monster
    boss_borders = [
        (13, 14), (13, 13), (14, 13), # Top left
        (13, 16), (13, 17), (14, 17), # Bottom left
        (17, 14), (17, 13), (16, 13), # Top right
        (17, 16), (17, 17), (16, 17) # Bottom right
    ]
    wb.add_rocks(
        len(boss_borders),
        locs=boss_borders
    )
    wb.add_monsters(
        [BossMonster()], locs=[(15, 15)]
    )

    return Game(
        world=wb.get_world(),
        players=wb.get_players(),
        max_rounds=max_rounds,
        win_condition='left_one_player'
    )
