#!/usr/bin/env python
import random
from pathlib import Path

from game.game import Game
from game.utils.main_utils import *
from game.world import World
from game.utils import *
from game.agent_ai import PLAYER_AIS, SimpleAI

from game_maps import *

class Client:
    def __init__(self, game: Game, save_dir = None):
        self.game = game
        self.character_default_settings = None
        self.world_code = None
        self.game_history = None
        self.endgame_stats = None
        self.save_dir = save_dir

    def run(self, auto_save=True):
        print(self.game.world)

        self.character_default_settings = get_default_settings(self.game.world)
        self.world_code = self.game.world.code.splitlines()

        # Start Game
        self.game.start()

        self.game_history = self.game.logger
        self.endgame_stats = self.game.world.endgame_stats

        if auto_save:
            self.save(self.save_dir)


    def save(self, directory=''):
        if directory:
            d = Path(directory)
            d.mkdir(exist_ok=True)
        else:
            d = Path()

        save_list(self.world_code, d / 'map_grid.txt')
        save_list(self.game_history, d / 'game_history.txt')

        save_default_settings(d / 'Character_Default_Settings.txt')
        save_player_info(d / 'playerinfo.txt', self.character_default_settings)
        save_end_statistics(d / 'results.txt', self.endgame_stats)


if __name__ == '__main__':
    from game.objects import Sword, Strawberry, Water, MonsterM, Player
    from game.terrain import Rock
    from world_builder import create_test_game, create_game_with_one_strong_monster_many_weak_monsters_and_two_imba_sword,\
        create_battle_royale

    # game = create_test_game()
    # game = create_game_with_one_strong_monster_many_weak_monsters_and_two_imba_sword()
    # game = create_battle_royale()
    game = create_arena_map(
        max_rounds=100,
        player_ais=[
            SimpleAI for _ in range(6)
        ]
    )
    client = Client(game=game)
    client.run(auto_save=False)
    client.save('./results')
