"""
File that stores static information to be used in the game
"""
#########################
#   Character Settings  #
#########################
# Unit types
ARCHER = 'archer'
MELEE = 'melee'
TANK = 'tank'
DEFAULT_UNIT = MELEE

# Agent codes
MELEE_CODE = 'Pk'
ARCHER_CODE = 'Pa'
TANK_CODE = 'Pt'
SLIME_CODE = 'Ms'
GOLEM_CODE = 'Mg'

# Parameter settings
MELEE_MAX_HEALTH = 100
MELEE_HEALTH = 100
ARCHER_MAX_HEALTH = 100
ARCHER_HEALTH = 100
TANK_MAX_HEALTH = 150
TANK_HEALTH = 150
MONSTER_MAX_HEALTH = 50
MONSTER_HEALTH = 50
BOSS_MONSTER_MAX_HEALTH = 100
BOSS_MONSTER_HEALTH = 100

# Hunger
INITIAL_MAX_HUNGER = 50
INITIAL_HUNGER = 50

# Default config
AGENT_CONFIG = {
    MELEE_CODE : {
        'maxHealth' : MELEE_MAX_HEALTH,
        'currHealth' : MELEE_HEALTH,
        'maxHunger': INITIAL_MAX_HUNGER,
        'currHunger' : INITIAL_HUNGER
    },
    ARCHER_CODE : {
        'maxHealth' : ARCHER_MAX_HEALTH,
        'currHealth' : ARCHER_HEALTH,
        'maxHunger': INITIAL_MAX_HUNGER,
        'currHunger' : INITIAL_HUNGER
    },
    TANK_CODE : {
        'maxHealth' : TANK_MAX_HEALTH,
        'currHealth' : TANK_HEALTH,
        'maxHunger': INITIAL_MAX_HUNGER,
        'currHunger' : INITIAL_HUNGER
    },
    SLIME_CODE : {
        'maxHealth' : MONSTER_MAX_HEALTH,
        'currHealth' : MONSTER_HEALTH
    },
    GOLEM_CODE : {
        'maxHealth' : BOSS_MONSTER_MAX_HEALTH,
        'currHealth' : BOSS_MONSTER_HEALTH
    }
}
PLAYER_AGENTS = [MELEE_CODE, ARCHER_CODE, TANK_CODE]

#######################
#   Inventory Items   #
#######################
# Object names
weapon_names = [
    'sword',
    'greatsword',
    'bow'
]

equipment_names = [
    'shoes', 'armor'
]

usable_names = [
    'strawberry',
    'water',
    'golden_apple'
]


# Item codes
ITEM_CODES = {
    '02' : 'water',
    '03' : 'strawberry',
    '04' : 'sword',
    '05' : 'bow',
    '06' : 'armor',
    '07' : 'shoe',
    '08' : 'golden apple'
}
