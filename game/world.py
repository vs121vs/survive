from __future__ import annotations

import warnings
from typing import Dict, List, Set, Tuple, Union, Optional, Type
import random
from matplotlib.pyplot import isinteractive

import numpy as np

from game.cell import Cell
from game.const import ITEM_CODES
from game.terrain import Terrain, Plain, Rock
from game.objects_base import WorldObject, InventoryItem, Weapon, LivingThing, \
    Agent, PointItem
from game.objects import BossMonster, MonsterM, Player, Water, Sword, GreatSword, Strawberry
from game.commands import Direction
from PathFinding.pathfind import PathFind
from PathFinding.search import astar_search

class World:
    """
    Game logic
    """

    def __init__(self, shape=(10, 10), add_border=True):
        _cells = [[Cell(pos=(r, c)) for c in range(shape[1])] for r in range(shape[0])]
        self.cell_arr = np.array(_cells)
        self.set_neighbours()
        self.indexes = np.moveaxis(np.indices(self.cell_arr.shape), 0, -1)

        self.agents: List[Agent] = list()
        self.objects: Set[WorldObject] = set()

        self.defeated_players: List[Player] = list()
        self.defeated_points: List[int] = [0, 10, 20, 30, 40, 50]
        self.endgame_stats: List[Dict] = list()

        self.id2obj = dict()

        if add_border:
            self.add_border()

    def add_border(self):
        border_idxs = self.border
        for pos in border_idxs:
            self.set_terrain(Rock, pos)

    def set_neighbours(self):
        xx, yy = self.shape
        for r in range(xx):
            for c in range(yy):
                # TODO: proper border neighbours
                cell = self.get_cell(r, c)
                if r != 0:
                    cell.up_cell = self.get_cell(r - 1, c)
                    cell.neighbour_cells.append(cell.up_cell)

                if r != xx - 1:
                    cell.down_cell = self.get_cell(r + 1, c)
                    cell.neighbour_cells.append(cell.down_cell)

                if c != yy - 1:
                    cell.left_cell = self.get_cell(r, c + 1)
                    cell.neighbour_cells.append(cell.left_cell)

                if c != 0:
                    cell.right_cell = self.get_cell(r, c - 1)
                    cell.neighbour_cells.append(cell.right_cell)

    @property
    def shape(self):
        return self.cell_arr.shape

    def get_neighbours(self, position):
        return self.get_cell(position).neighbour_cells

    @property
    def border(self):
        out = []
        out.extend(self.indexes[:, 0].tolist())
        out.extend(self.indexes[:, -1].tolist())
        out.extend(self.indexes[0, 1:-1].tolist())
        out.extend(self.indexes[-1, 1:-1].tolist())
        return out

    def set_terrain(self, terrain, pos):
        return self.get_cell(pos).set_terrain(terrain)

    # Objects
    def add_object(self, obj, pos):
        if isinstance(pos, Cell):
            pos = pos.pos
        assert obj not in self.objects, obj
        cell = self.cell_arr[tuple(pos)]
        self.id2obj[obj.uid] = obj
        self.objects.add(obj)
        return cell.add_object(obj)

    def remove_object(self, obj):
        assert obj in self.objects, obj
        obj.parent_cell.remove_object(obj)
        self.objects.remove(obj)
        return True

    def move_object_to(self, obj: WorldObject, pos):
        pos = tuple(pos)
        cur_cell = obj.parent_cell
        dest_cell = self.get_cell(pos)
        if cur_cell == dest_cell:
            return False
        if dest_cell.blocker and obj.pos != pos:
            warnings.warn(f'destination blocked by {dest_cell.blocker}')
            return False
        if self.distance(obj, pos) > obj.movement:
            return False

        cur_cell.remove_object(obj)
        dest_cell.add_object(obj)
        return True

    # def can_move(self, obj, displacement):
    #     if isinstance(displacement, Direction):
    #         displacement = displacement.value
    #     if sum(displacement) > obj.movement:
    #         return False
    #     new_pos = obj.parent_cell.get_displacement_pos(displacement)
    #     has_path = self.get_shortest_path_to(obj, new_pos)
    #     return (has_path is not None)

    def can_move(self, obj, displacement):
        if isinstance(displacement, Direction):
            displacement = displacement.value
        if sum(displacement) > obj.movement:
            return False
        new_pos = obj.parent_cell.get_displacement_pos(displacement)
        try:
            cell = self.get_cell(new_pos)
        except IndexError: # Quick hack for out of bounds movement
            return False

        if cell.blocker:
            return False
        return True

    def get_shortest_path_to(self, obj, dest, unblock_dest=True):
        grid = self.encode_cell_arr_into_bool_arr()
        #make dest empty space
        if unblock_dest:
            grid[dest[0]][dest[1]] = True
        prob = PathFind(grid, obj.pos, dest)
        search_tree = astar_search(prob)
        path = search_tree.solution() if search_tree else None
        return path

    # convenience method
    #encode cell array into a 2D boolean array, wherein True is empty space and False is obstacle
    def encode_cell_arr_into_bool_arr(self):
        grid = []
        for row in self.cell_arr:
                tmp = []
                for cell in row:
                        tmp.append(not cell.blocker) # append true if empty space, false if obstacle
                grid.append(tmp)
        return grid

    def move_object(self, obj: WorldObject, displacement: Tuple[int, int]):
        if sum(displacement) > obj.movement:
            return False
        new_pos = obj.parent_cell.get_displacement_pos(displacement)
        return self.move_object_to(obj, new_pos)

    def get_agent_snapshot(self, agent: Agent):
        """
        @ Wey Yeh: Modified to add more statistics for testing and debugging
        """
        inv = agent.inventory
        out = dict(
            rank=None,
            player_id=agent.code,
            player_name=agent.name,
            player_ai=agent.agent_ai.__class__.__name__,
            points=agent.gathered_points,
            health=agent.health,
            hunger=agent.hunger,
            inventory={}
        )

        if hasattr(agent, 'unit_type'):
            out['unit_type'] = agent.unit_type
        
        if hasattr(agent, 'player_kills'):
            out['player_kills'] = agent.player_kills
        if hasattr(agent, 'slime_kills'):
            out['slime_kills'] = agent.slime_kills
        if hasattr(agent, 'golem_kills'):
            out['golem_kills'] = agent.golem_kills
        
        # Collate inventory statistics
        for item in inv:
            if item.code in out['inventory']:
                out['inventory'][item.code] += 1
            else:
                out['inventory'][item.code] = 1
        
        for item_code in ITEM_CODES:
            if item_code not in out['inventory']:
                out['inventory'][item_code] = 0
            
        return out

    def kill_agent(self, agent, logger=None, attacker=None):
        logger = logger if logger is not None else []

        # Points handling
        if isinstance(agent, Player):
            self.add_standing_points(agent, logger=logger)

            self.defeated_players.append(agent)
            self.endgame_stats.append(self.get_agent_snapshot(agent))

        # Remove point items
        point_items = [item for item in agent.inventory if isinstance(item, PointItem)]
        for item in point_items:
            agent.remove_from_inventory(item)
        
        # Add to kill count for players
        if attacker and isinstance(attacker, Player):
            if isinstance(agent, Player):
                attacker.player_kills += 1
            elif isinstance(agent, BossMonster):
                attacker.golem_kills += 1
            else:
                attacker.slime_kills += 1

        self.remove_agent(agent)

        def_row, def_col = agent.pos
        logger.append(f'{agent.code}, DEAD, {def_row}_{def_col}, 00')
        
        return logger

    def drop_items(self, attacker, defender, logger=None):
        """
        @Author: Wey Yeh
        Function that drops items from monsters into the player's inventory upon defeat
        """
        logger = logger if logger is not None else []
        # Sanity checks
        assert defender.health <= 0
        
        def_row, def_col = defender.pos

        if isinstance(attacker, Player):
            # Monster case
            if isinstance(defender, MonsterM):
                # Select from fixed item drop, or set of items dropped by monster
                item_type = defender.dropped_item if defender.dropped_item is not None else random.choice(defender.drop_list)
                item = item_type()
                # Add item to world directory as well
                self.id2obj[item.uid] = item
                
                attacker.add_to_inventory(item)
                item_count = len([item for item in attacker.inventory if isinstance(item, item_type)])
                
                logger.append(f'{attacker.code}, PICK, 1, {item.code}, {def_row}_{def_col}, {item_count}, F')
            
            # Player case 
            elif isinstance(defender, Player):
                # Additional check to make sure no point item drop
                drop_items = [item for item in defender.inventory if not isinstance(item, PointItem)]
                # Adds defeated player's items to the attacker
                added_items = {}
                code_to_type = {}
                for item in drop_items:
                    defender.remove_from_inventory(item)
                    attacker.add_to_inventory(item)

                    if item.code in added_items:
                        added_items[item.code] += 1
                    else:
                        added_items[item.code] = 1

                    code_to_type[item.code] = type(item)
                
                # Log added items
                for item_code in added_items:
                    item_type = code_to_type[item_code]
                    item_count = added_items[item_code]
                    total_count = len([item for item in attacker.inventory if isinstance(item, item_type)])
                    
                    logger.append(f'{attacker.code}, PICK, {item_count}, {item.code}, {def_row}_{def_col}, {total_count}, F')
        
        else:
            # TODO: Case if attacker is monster - Drop player items?
            pass

        return logger

    def add_standing_points(self, agent, logger=None):
        logger = logger if logger is not None else []
        standing_points = self.defeated_points[len(self.defeated_players)]
        self.add_points(agent, standing_points, logger)
        return logger

    def add_points(self, agent, points, logger=None):
        logger = logger if logger is not None else []
        if not isinstance(agent, Player):
            return logger

        agent.gathered_points += points
        if points == 0:
            return

        if points > 0:
            logger.append(f'{agent.code}, SCORE, PLUS, {points}, {agent.gathered_points}')
            print(f'{agent.name} gained {points} points!')
        else:
            logger.append(f'{agent.code}, SCORE, MINUS, {points}, {agent.gathered_points}')
            print(f'{agent.name} lost {points} points!')
        return logger

    def add_agent(self, agent, pos):
        assert agent.parent_cell is None, agent
        pos = tuple(pos)
        if not self.add_object(agent, pos):
            assert False, pos
        self.agents.append(agent)

    def remove_agent(self, agent):
        self.agents.remove(agent)
        self.remove_object(agent)

    def distance(self, ref1, ref2):
        refs = [ref1, ref2]
        refs = [ref.pos if isinstance(ref, WorldObject) else ref for ref in refs]
        refs = [ref.pos if isinstance(ref, Cell) else ref for ref in refs]
        pos_a, pos_b = refs
        return abs(pos_a[0] - pos_b[0]) + abs(pos_a[1] - pos_b[1])

    def displacement(self, ref1, ref2):
        refs = [ref1, ref2]
        refs = [ref.pos if isinstance(ref, WorldObject) else ref for ref in refs]
        refs = [ref.pos if isinstance(ref, Cell) else ref for ref in refs]
        pos_a, pos_b = refs
        return pos_b[0] - pos_a[0], pos_b[1] - pos_a[1]

    def get_cell(self, idx, y=None) -> Cell:
        if y is not None:
            return self.cell_arr[(idx, y)]
        return self.cell_arr[tuple(idx)]

    def view_lst(self):
        out = []
        for cell_row in self.cell_arr:
            sub_out = []
            for cell in cell_row:
                sub_out.append(cell.symbol)
            out.append(' '.join(sub_out))
        return out

    def __str__(self):
        return '\n'.join(self.view_lst())

    @property
    def code(self):
        out = []
        for cell_row in self.cell_arr:
            sub_out = []
            for cell in cell_row:
                sub_out.append(cell.code)
            out.append(' '.join(sub_out))
        return '\n'.join(out)

    @property
    def alive_agents(self):
        return [agent for agent in self.agents if not agent.is_dead]

    @property
    def character_settings(self):
        settings = []
        agent_types = set()
        for agent in self.agents:
            if agent.__class__ not in agent_types:
                settings.append(agent.character_settings)
                agent_types.add(agent.__class__)
        return '\n'.join(settings)

    def __contains__(self, item):
        return item in self.agents or item in self.objects
