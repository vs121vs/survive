class Terrain:
  blocking: bool = False
  symbol: str = 'x'
  code: str = '??'


class Plain(Terrain):
  symbol: str = ' '
  code: str = '00'


class Rock(Terrain):
  blocking: bool = True
  symbol: str = '^'
  code: str = '01'

# Unused
# class Grass(Terrain):
#   symbol: str = 'v'
#
#
# class Water(Terrain):
#   blocking: bool = True
#   symbol: str = 'w'
