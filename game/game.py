from __future__ import annotations
from dataclasses import dataclass, field
from typing import TYPE_CHECKING, Tuple, List
import random
import copy
import traceback

from prettyprinter import pprint, install_extras

from game.world import World
from game.commands import IdleCommand, CommandException, Direction, Command
from game.objects_base import InventoryItem, PointItem, UsableItem, Agent, LivingThing
from game.objects import MonsterM

from func_timeout import FunctionTimedOut

install_extras(
        # Comment out any packages you are not using.
        include=[
                'dataclasses',
        ],
        warn_on_error=True
)


def before_and_after(before, after):
    out = [b + '     ->     ' + a for b, a in zip(before, after)]
    return '\n'.join(out)

class Game:
    def __init__(self, world, players, max_rounds=100, logging=None, win_condition='left_one_player'):
        self.world: World = world
        self.players = set(players)
        self.prev_states = dict()

        self.round_num = 1
        self.max_rounds = max_rounds
        self.logger = logging or []
        self.endgame_logger = []
        self.win_condition = win_condition

    def start(self):
        """
        Starts a full game.

        A full game usually consists of multiple rounds. The player with the most points
         at the end is the winner.
        """
        # initialize prev_states
        for agent in self.world.agents:
            self.prev_states[agent] = self.get_agent_view(agent)

        self.logger.append('START GAME')
        while not self.game_ended():
            round_msg = f'Round #{self.round_num}'
            print('=' * len(round_msg))
            print(round_msg)
            print('=' * len(round_msg))
            self.play_round()
            self.round_num += 1

        for player in self.players:
            if not player.is_dead:
                self.world.add_standing_points(player, self.logger)
                
                agent_stats = self.world.get_agent_snapshot(player)
                
                self.world.endgame_stats.append(
                    agent_stats
                )

        print()
        print(f'===========')
        print(f'Final State')
        print(f'===========')
        print(self.world)
        self.announce_overall_winner()
        self.logger.append('END GAME')

        self.world.endgame_stats.sort(key=lambda x: x['points'], reverse=True)
        for i, player_stats in enumerate(self.world.endgame_stats, start=1):
            player_stats['rank'] = i
        self.endgame_logger = [','.join(map(str, stats.values())) for stats in self.world.endgame_stats]

        return self.world.endgame_stats

    def play_round(self):
        """
        Play a round to completion.

        """
        self.logger.append(f'START ROUND {self.round_num}')

        agents = list(self.world.agents)
        # [removed] shuffle agents at the start of each round
        # random.shuffle(agents)
        # ordered_codes = [agent.code for agent in agents if agent in self.players]
        # self.logger.append(f'START ROUND ORDER {", ".join(ordered_codes)}')
        print(f'[Round Order] {[agent.name for agent in agents]}')

        for agent in list(agents):
            if agent.is_dead:
                continue

            print()
            turn_msg = f"{agent.name}'s turn"
            print('-' * len(turn_msg))
            print(turn_msg)
            print('-' * len(turn_msg))
            self.logger.append(f'{agent.code}, START_TURN')

            before = self.world.view_lst()

            # Hunger decrement
            if agent.hunger_delta:
                new_hunger = max(0, agent.hunger - agent.hunger_delta)
                delta = agent.hunger - new_hunger
                agent.hunger = new_hunger
                self.logger.append(f'{agent.code}, HUNGER, MINUS, {delta}, {agent.hunger}')

            # Agent dead from hunger
            if agent.hunger <= 0:
                print(f"[Died from hunger] {agent.name}")
                self.world.kill_agent(agent, self.logger)

            if agent.is_dead:
                after = self.world.view_lst()
                print()
                print(before_and_after(before, after))
                self.logger.append(f'{agent.code}, END_TURN')
                continue

            agent_view = self.get_agent_view(agent)

            command = None
            try:
                prev_state = self.prev_states[agent]
                self.prev_states[agent] = agent_view
                command = agent.decide(agent_view, prev_state=prev_state)
                if not isinstance(command, Command):
                    command = IdleCommand()
                command.world = self.world
                command.agent = agent

                command.execute()
            except CommandException as e:
                print("Player's decide function encountered Command exception.", e)
                # traceback.print_exception(type(e), e, e.__traceback__)
                # exit(1)

                command = IdleCommand()
                command.world = self.world
                command.agent = agent

                command.execute()
            except FunctionTimedOut:
                print("Player's decide function timed out and was terminated.")

                command = IdleCommand()
                command.world = self.world
                command.agent = agent

                command.execute()
            except Exception as e:
                print("Player's decide function encountered exception.", e)
                # traceback.print_exception(type(e), e, e.__traceback__)
                # exit(1)

                command = IdleCommand()
                command.world = self.world
                command.agent = agent

                command.execute()


            after = self.world.view_lst()
            print()
            print(before_and_after(before, after))
            print()
            print('[Command Parameters]')
            pprint(command)
            print()
            self.logger.extend(command.logger)
            self.logger.append(f'{agent.code}, END_TURN')

        # self.logger.append(f'END OF ROUND')

    def get_agent_view(self, agent: Agent):
        view = dict()
        view['self'] = agent.as_dict()

        # attackables
        """
        @ Wey Yeh: Modified this section to use get_agent_attackables
        """
        direct_attackables, direct_attackable_uids = self.get_agent_attackables(agent)
        view['attackables'] = direct_attackables

        indirect_attackables = []
        for obj in self.world.agents:
            obj_dict = obj.as_dict()
            if obj != agent and hasattr(obj, 'health') and (obj.uid not in direct_attackable_uids):
                shortest_path = self.world.get_shortest_path_to(agent, obj.pos)
                if shortest_path:
                    obj_dict['shortest_path'] = shortest_path
                    indirect_attackables.append(obj_dict)
        view['indirect_attackables'] = indirect_attackables

        # takable
        view['takeables'] = [item.as_dict() for item in agent.parent_cell.objects
                            if isinstance(item, InventoryItem)]
        takable_uids = {item['uid'] for item in view['takeables']}

        indirect_takeables = []
        for obj in self.world.objects:
            if not isinstance(obj, InventoryItem) or obj.uid in takable_uids:
                continue

            # Skip objects where an agent is currently on that same location
            if obj.parent_cell.blocker:
                continue

            shortest_path = self.world.get_shortest_path_to(agent, obj.pos)
            if shortest_path:
                item = obj.as_dict()
                item['shortest_path'] = shortest_path
                indirect_takeables.append(item)

        view['indirect_takeables'] = indirect_takeables

        # useables
        view['usables'] = [item.as_dict() for item in agent.inventory if isinstance(item, UsableItem)]

        # movable
        view['movables'] = [d for d in Direction if self.world.can_move(agent, d)]

        return view

    def get_agent_attackables(self, agent):
        """
        Obtains all attackable targets within agent's attack range
        """
        agent_pos = agent.parent_cell.pos
        attack_range = agent.attack_range
        x_min, y_min, x_max, y_max = 1, 1, self.world.shape[0] - 2, self.world.shape[1] - 2

        # Generate all possible attacking grids
        attackable_coordinates = set()
        for i in range(1, attack_range + 1):
            for j in range(0, attack_range - i + 1):
                # All combinations of movement for the changes in x and y axes (i, j)
                attackable_coordinates.add((agent_pos[0] + i, agent_pos[1]  + j))
                attackable_coordinates.add((agent_pos[0] + i, agent_pos[1]  - j))
                attackable_coordinates.add((agent_pos[0] - i, agent_pos[1]  + j))
                attackable_coordinates.add((agent_pos[0] - i, agent_pos[1]  - j))
                # Account for up down axis
                attackable_coordinates.add((agent_pos[0] + j, agent_pos[1]  + i))
                attackable_coordinates.add((agent_pos[0] + j, agent_pos[1]  - i))
                attackable_coordinates.add((agent_pos[0] - j, agent_pos[1]  + i))
                attackable_coordinates.add((agent_pos[0] - j, agent_pos[1]  - i))

        # Boundary check
        attackable_coordinates = [
            (x, y) for x, y in attackable_coordinates if (
                (x >= x_min and x <= x_max) and 
                (y >= y_min and y <= y_max)
            )
        ]

        direct_attackables = []
        direct_attackable_uids = dict()
        for position in attackable_coordinates:
            cell = self.world.get_cell(position)
            for obj in cell.objects:
                if hasattr(obj, 'health'):
                    direct_attackables.append(obj.as_dict())
                    direct_attackable_uids[obj.uid] = None
    
        return direct_attackables, direct_attackable_uids

    def game_ended(self):
        """
        Determines if the game has ended based on several termination conditions
        @Modified: Wey Yeh
        """
        # No more rounds
        if self.round_num >= self.max_rounds:
            return True

        # Edge case - No players left
        num_of_alive = len(self.players) - sum(1 for p in self.players if p.is_dead)
        if num_of_alive <= 0:
            return True

        # Last player standing
        if self.win_condition == 'left_one_player':
            return num_of_alive <= 1

        # All monsters defeated
        elif self.win_condition == 'no_monsters_left':
            monsters = [agent for agent in self.world.agents if isinstance(agent, MonsterM)]
            return len(monsters) <= 0
        
        # No items remaining
        elif self.win_condition == 'no_items_left':
            objects = [obj for obj in self.world.objects if not isinstance(obj, LivingThing)]
            return len(objects) <= 0

        # No more rounds remaining or no players left
        else:
            return self.round_num >= self.max_rounds

    def announce_overall_winner(self):
        print('=====')
        print('Score')
        print('=====')
        for player in self.players:
            # @ Wey Yeh: Added AgentAI type to see comparison between different strategies
            print('Player {}, {} scored {}'.format(player.name, player.agent_ai, player.gathered_points))
