from __future__ import annotations

from dataclasses import InitVar, dataclass, field
from typing import Type

from func_timeout import func_set_timeout
from prettyprinter import pprint

from game.agent_ai import AgentAI, MonsterSimpleAI, SimpleAI
from game.objects_base import WorldObject, Weapon, UsableItem, PointItem, Agent, Equipment
from game.const import *

#########################
# Weapons and Equipment #
#########################
@dataclass(eq=False)
class Sword(Weapon):
    name: str = 'sword'
    atk: int = 10
    symbol: str = 's'
    code: str = '04'


@dataclass(eq=False)
class GreatSword(Weapon):
    name: str = 'greatsword'
    atk: int = 20
    symbol: str = 'S'
    code: str = '06'

@dataclass(eq=False)
class Shoes(Equipment):
    """
    Shoes equipment that gives bonus movement speed
    """
    name: str = 'shoes'
    symbol: str = 'r'
    code: str = '07'
    movement_boost: int = 1

@dataclass(eq=False)
class Armor(Equipment):
    """
    Armor equipment that provides damage reduction per hit, but at the cost of reduced movement
    """
    name: str = 'armor'
    symbol: str = 'a'
    code: str = '06'
    movement_boost: int = -1
    damage_reduction: int = 5

@dataclass(eq=False)
class Bow(Weapon):
    name: str = 'bow'
    atk: int = 5
    attack_range_boost: int = 1
    symbol: str = 'B'
    code: str = '05'

###################
#   Usable Items  #
###################

@dataclass(eq=False)
class Strawberry(PointItem):
    name: str = 'strawberry'
    symbol: str = 'b'
    code: str = '03'
    health_gain: int = 20
    hunger_gain: int = 10
    points: int = 10


@dataclass(eq=False)
class Water(UsableItem):
    name: str = 'water'
    symbol: str = 'w'
    code: str = '02'
    health_gain: int = 30
    hunger_gain: int = 15

@dataclass(eq=False)
class GoldenApple(UsableItem):
    name: str = 'golden apple'
    symbol: str = 'g'
    code: str = '08'
    health_gain: int = 100
    hunger_gain: int = 25

##########
# Agents #
# ########
@dataclass(eq=False)
class MonsterM(Agent):
    monster_id: int = field(default=0, repr=False)
    movement: int = 1
    symbol: str = 'Ms'
    name: str = 'monster'
    base_atk: int = 15
    health: int = MONSTER_MAX_HEALTH
    hunger_delta: int = 0
    points: int = 30
    gathered_points: int = 0
    agent_ai: Type[AgentAI] = MonsterSimpleAI

    drop_list = [Shoes, Armor, Bow, Sword] # Default list of items to drop

    def __post_init__(self, parent_cell, timeout=None):
        super().__post_init__(parent_cell, timeout=timeout)
        self.code = f'{self.symbol}{self.monster_id}'
        self.name = f'{self.name}_{self.code}'
        self.dropped_item = None

    @property
    def character_settings(self):
        out = list()
        out.append(f'Start of {self.symbol}')

        out.append(f'Health: {self.health}')
        out.append(f'Hunger: {self.hunger}')

        out.append(f'End')
        return '\n'.join(out)

    @property
    def atk(self):
        return self.base_atk

    @property
    def attack_range(self):
        """
        Fixed attack range for monster
        """
        return self.base_attack_range # defined as 1 in Agent class

    @property
    def total_movement(self):
        bonus_movement = 0
        for item in self.inventory:
            if isinstance(item, Equipment):
                bonus_movement += item.movement_boost

        return self.movement + bonus_movement

@dataclass(eq=False)
class BossMonster(MonsterM):
        points: int = 50
        base_atk: int = 20
        symbol: str = 'Mg'
        movement: int = 2
        name: str = 'boss_monster'
        health: int = BOSS_MONSTER_MAX_HEALTH

        drop_list = [GoldenApple]

        def __post_init__(self, parent_cell, timeout=1):
            super().__post_init__(parent_cell, timeout)
            self.dropped_item = None

@dataclass(eq=False)
class Player(Agent):
    player_id: int = field(default='0')
    symbol: str = field(default='P')
    movement: int = 2
    base_attack_range: int = 1
    base_atk: int = 10
    health: int = 100
    hunger: int = INITIAL_HUNGER
    hunger_delta: int = 1
    max_hunger: int = INITIAL_MAX_HUNGER
    unit_type = DEFAULT_UNIT # default unit_type

    # Player statistics
    player_kills = 0
    slime_kills = 0
    golem_kills = 0

    points: int = 75
    gathered_points: int = 0
    timeout: InitVar[int] = 1

    def __post_init__(self, parent_cell, timeout=1):
        super().__post_init__(parent_cell, timeout=timeout)

        # special abilities
        unit_type = self.agent_ai.unit_type
        if unit_type == MELEE:
            self.health = MELEE_HEALTH
            self.max_health = MELEE_MAX_HEALTH
            self.base_attack_range = 1
            self.base_atk = 10
            self.movement = 2
            self.base_damage_reduction = 0
            self.unit_type = unit_type
            self.symbol = 'Pk'
        elif unit_type == ARCHER:
            self.health = ARCHER_HEALTH
            self.max_health = ARCHER_MAX_HEALTH
            self.base_attack_range = 3
            self.base_atk = 5
            self.movement = 2
            self.base_damage_reduction = 0
            self.unit_type = unit_type
            self.symbol = 'Pa'
        elif unit_type == TANK:
            self.health = TANK_HEALTH
            self.max_health = TANK_MAX_HEALTH
            self.base_atk = 10
            self.base_attack_range = 1
            self.movement = 1
            self.unit_type = unit_type
            self.base_damage_reduction = 5
            self.symbol = 'Pt'
        else:
            print("Unit type not found, allowable types include (melee, archer, tank), using default unit type:", self.unit_type)

        self.code = f'{self.symbol}{self.player_id}'
        self.symbol = f'{self.player_id}'

        if not self.name:
            self.name = f'Team{self.player_id}'

        print()
        print(f"Player {self.player_id} created with the following configuration:")
        pprint(vars(self))
        print()

    @func_set_timeout(1)
    def decide(self, state):
        return super().decide(state)

    @property
    def character_settings(self):
        out = list()
        out.append(f'Start of P')

        out.append(f'unitType: {self.agent_ai.unit_type}')
        out.append(f'maxHealth: {self.max_health}')
        out.append(f'currHealth: {self.health}')
        out.append(f'maxHunger: {self.max_hunger}')
        out.append(f'currHunger: {self.hunger}')
        out.append(f'Score: {self.gathered_points}')

        num_of_item_types = {"02":0,"03":0,"04":0,"06":0}
        for i in self.inventory:
            num_of_item_types[i.code]+=1
        for (k,v) in num_of_item_types.items():
            out.append(f'{k}: {v}')
        out.append(f'Inventory: {", ".join(num_of_item_types.keys())}')
        out.append(f'End')
        return '\n'.join(out)

    @property
    def atk(self):
        bonus_atk = sum([item.atk for item in self.inventory if isinstance(item, Weapon)])
        return self.base_atk + bonus_atk

    @property
    def attack_range(self):
        """
        NOTE: Does not appear in as_dict(), may need to manually override here to add it in
        """
        attack_range = self.base_attack_range
        for item in self.inventory:
            if hasattr(item, 'attack_range_boost'):
                attack_range += item.attack_range_boost
        
        return attack_range

    @property
    def damage_reduction(self):
        """
        Amount of damage reduced when computing damage calculations
        """
        damage_reduction = self.base_damage_reduction
        for item in self.inventory:
            if hasattr(item, 'damage_reduction'):
                damage_reduction += item.damage_reduction

        return damage_reduction

    @property
    def total_movement(self):
        bonus_movement = 0
        for item in self.inventory:
            if isinstance(item, Equipment):
                bonus_movement += item.movement_boost

        return self.movement + bonus_movement
