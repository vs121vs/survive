from __future__ import annotations

from dataclasses import dataclass, field
from typing import List, Set, Tuple, Union, Optional, Type


from game.terrain import Terrain, Plain
from game.objects_base import WorldObject, InventoryItem, Weapon, LivingThing, \
  Agent


@dataclass
class Cell:
  terrain: Terrain = field(default=Plain)
  objects: Set[WorldObject] = field(default_factory=set)
  pos: Tuple = field(default=None)
  has_chest: bool = False

  # post_init
  blocker: Optional[Union[Terrain, WorldObject]] = field(default=None, init=False)

  def __post_init__(self):
    self.up_cell = None
    self.down_cell = None
    self.left_cell = None
    self.right_cell = None
    self.neighbour_cells = []

    if self.terrain.blocking:
      self.blocker = self.terrain
    blocking_objs = [obj for obj in self.objects if obj.blocking]

    if len(blocking_objs) > 1 or (blocking_objs and self.blocker):
      assert False, self.objects

    if blocking_objs:
      self.blocker = blocking_objs[0]

  def add_object(self, obj: WorldObject):
    if obj.blocking and self.blocker:
      raise Exception()
    self.objects.add(obj)
    if obj.blocking:
      self.blocker = obj
    obj.set_parent_cell(self)
    return True

  def remove_object(self, obj):
    assert obj in self.objects
    self.objects.remove(obj)
    if obj.blocking:
      self.blocker = None
    return True

  def set_terrain(self, terrain: Terrain):
    if self.blocker and (terrain.blocking and not self.terrain.blocking):
      assert False, self.blocker
    self.terrain = terrain

    if terrain.blocking:
      self.blocker = terrain

  @property
  def symbol(self) -> str:
    s = self.terrain.symbol
    if self.blocker:
      s = self.blocker.symbol
    else:
      for obj in self.objects:
        s = obj.symbol
    return s

  @property
  def code(self) -> str:
    code = self.terrain.code
    if self.blocker:
      code = self.blocker.code
    else:
      for obj in self.objects:
        code = obj.code
    return code

  def get_displacement_pos(self, displacement: Tuple[int, int]):
    assert len(displacement) == 2, displacement
    return tuple(x + d for d, x in zip(displacement, self.pos))

  def get_any_of_type(self, obj_type: Type[WorldObject]):
    for obj in self.objects:
      if isinstance(obj, obj_type):
        return obj
    return None
