"""
@author: Vincent Seng

Supports non-pickle-able functions.
"""
from multiprocessing import Process, Queue, JoinableQueue, Pool
import time
from tqdm import tqdm
import heapq
import random

_STOP_TOKEN = 'STOP'


#############
# Interface #
#############
def run_multiproc(fn, tasks, worker_num, multi_arg=False, verbose=False,
                  ordered=True, initialize_fn=False, pbar=False,
                  shuffle_execution=False, no_args=False, tqdm_smoothing=0):
  """
  out = run_multiproc(fn=lambda x: x*2,
                      tasks=[i for i in range(10)],
                      worker_num=4,
                      multi_arg=False)
  print(out)
  """
  if no_args:
    tasks = range(tasks) if isinstance(tasks, int) else tasks
    tasks = [() for _ in tasks]
    multi_arg = True

  num_tasks = len(tasks)
  task_queue = JoinableQueue()
  out_queue = Queue()

  indexes = None
  if shuffle_execution:
    indexes = [i for i in range(num_tasks)]
    random.shuffle(indexes)
    tasks = [tasks[i] for i in indexes]

  _add_tasks(task_queue, tasks)
  if verbose:
    print('Creating {} worker~process'.format(worker_num))
  workers = [Worker(fn=fn,
                    in_queue=task_queue,
                    out_queue=out_queue,
                    worker_id=w_id,
                    multi_arg=multi_arg,
                    initialize_fn=initialize_fn,
                    verbose=verbose)
             for w_id in range(worker_num)]
  procs = [Process(target=worker) for worker in workers]

  for proc in procs:
    proc.start()

  _iter = range(num_tasks)
  if verbose or pbar:
    _iter = tqdm(range(num_tasks),
                 desc=f'[run_multiproc {worker_num}x]',
                 smoothing=tqdm_smoothing)

  out = [out_queue.get() for _ in _iter]

  if ordered:
    out = sorted(out, key=lambda x: x[1])
  out = [item[0] for item in out]

  if shuffle_execution:
    out = sorted([(i, item) for i, item in zip(indexes, out)])
    out = [item[1] for item in out]

  # Join processes
  _add_stop_tokens(task_queue, worker_num)
  for proc in procs:
    proc.join()
  task_queue.join()

  return out


def multi_map(fn, tasks, queue_size, worker_num, multi_arg=False,
              ordered_tasks=False, verbose=False):
  num_tasks = len(tasks)
  task_queue = JoinableQueue()
  out_queue = Queue(queue_size)

  _add_tasks(task_queue, tasks)
  _add_stop_tokens(task_queue, worker_num)
  if verbose:
    print('Creating {} worker~process'.format(worker_num))

  workers = [Worker(fn, task_queue, out_queue, w_id, multi_arg, verbose)
             for w_id in range(worker_num)]

  procs = [Process(target=worker) for worker in workers]
  for proc in procs:
    proc.start()

  if ordered_tasks:
    my_queue = _OrderedQueue(out_queue, num_tasks, [*procs, task_queue])
  else:
    my_queue = _Queue(out_queue, num_tasks, [*procs, task_queue])
  return my_queue


##########
# worker #
##########
class Worker:
  def __init__(self, fn, in_queue, out_queue, worker_id, multi_arg=False,
               initialize_fn=False, verbose=False, debug=False):
    self.fn = fn() if initialize_fn else fn
    self.in_queue = in_queue
    self.out_queue = out_queue
    self.worker_id = worker_id
    self.multi_arg = multi_arg
    self.verbose = verbose
    self.debug = debug

    if self.multi_arg:
      self.fn = lambda x: fn(*x)

  def __call__(self):
    if self.debug:
      print('worker', self.worker_id, 'running')
    while True:
      item, task_id = self.in_queue.get()
      if item == _STOP_TOKEN:
        self.in_queue.task_done()
        if self.debug:
          print('[Worker {}] Stop signal received. Job\'s done'
                .format(self.worker_id))
        return
      self.out_queue.put((self.fn(item), task_id))
      self.in_queue.task_done()


####################
# production queue #
####################
class _Queue:
  def __init__(self, queue, total, joinables):
    self.q = queue
    self.total = total
    self.joinables = joinables

  def __iter__(self):
    self.i = 0
    return self

  def __next__(self):
    if self.i == self.total:
      for joinable in self.joinables:
        joinable.join()
      raise StopIteration
    self.i += 1
    return self.q.get()[0]


class _OrderedQueue(_Queue):
  def __init__(self, queue, total, joinables):
    super().__init__(queue, total, joinables)
    self.pq = []

  def __iter__(self):
    self.i = 0
    self.retreived_count = 0
    return self

  def __next__(self):
    # TODO: Check edge cases
    if self.i == self.total:
      self._terminate()

    while True:
      if self.retreived_count < self.total:
        item, task_id = self.q.get()
        self.retreived_count += 1
        heapq.heappush(self.pq, (task_id, item))
      if self.pq and self.pq[0][0] == self.i:
        self.i += 1
        return heapq.heappop(self.pq)[1]

  def _terminate(self):
    for joinable in self.joinables:
      joinable.join()
    raise StopIteration


###########
# control #
###########
def _add_stop_tokens(task_queue, worker_num):
  for _ in range(worker_num):
    task_queue.put((_STOP_TOKEN, None))


def _add_tasks(task_queue, tasks):
  for i, task in enumerate(tasks):
    task_queue.put((task, i))


if __name__ == '__main__':
  def foo(task):
    print("[foo] task {} received".format(task))
    time.sleep(1)
    return task

  tic = time.time()
  with Pool(processes=4) as pool:
    xx = pool.imap(foo, range(10), chunksize=2)
    for i in xx:
      print(i, time.time() - tic)

  print(time.time() - tic)

  def boo(task):
    return task * 2

  booboo = lambda x: x*2
  zoo = lambda x: None

  tasks = [i for i in range(10)]
  out = run_multiproc(zoo, tasks, 4, multi_arg=False)
  print(out)

  prod_queue = multi_map(foo, [i for i in range(5)], 2, 1, False)

  tic = time.time()
  print(prod_queue)
  for item in prod_queue:
    time.sleep(1)
    print(item)
  print(time.time()-tic)

  tic = time.time()
  for task in [i for i in range(5)]:
    item = foo(task)
    time.sleep(1)
    print(item)
  print(time.time()-tic)

  def goo(task):
    print(task)
    return task

  run_multiproc(goo, [1,2,3,4,5,6], shuffle_execution=True, worker_num=2)