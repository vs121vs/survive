import random

from game.const import *

"""
Helper functions for game decision making
"""
def filter_weapons(items):
    """
    Filters only weapons in list of items
    """
    return [
        x for x in items if x['name'] in weapon_names
    ]

def filter_equipment(items):
    """
    Filters only equipment in list of items
    """
    return [
        x for x in items if x['name'] in equipment_names
    ]

def filter_usables(items):
    """
    Filters only usables in list of items
    """
    return [
        x for x in items if x['name'] in usable_names
    ]

def get_target_distances(targets, agent_pos):
    """
    Get the distance between the agent and each attackable target
    """
    return [
        abs(agent_pos[0] - x['position'][0]) + abs(agent_pos[1] - x['position'][1]) for x in targets
    ]

def get_nearest_item(items):
    """
    Get the nearest item based on the shortest path
    """
    sorted_items = sorted(
        items, key=lambda x : len(x['shortest_path'])
    )
    return sorted_items[0]

def get_nearest_target(targets):
    """
    Get nearest target based on shortest path. Ties broken on lowest health
    """
    sorted_targets = sorted(
        targets, key=lambda x : (len(x['shortest_path']), x['health'])
    )
    return sorted_targets[0]

def get_movement_directions(path, movement):
    """
    Get movement directions based on path and agent movement speed
    """
    steps = min(
        movement,
        len(path)
    )
    return path[:steps]

def get_player_targets(targets):
    """
    Gets list of player attackables from list of attackables
    """

    return [
        x for x in targets if 'player_id' in x
    ]

def get_monster_attackables(targets):
    """
    Gets list of monster attackables from list of attackables
    """

    return [
        x for x in targets if 'monster_id' in x
    ]
