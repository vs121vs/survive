from __future__ import annotations
import random

from game.commands import Command, AttackCommand, TakeItemCommand, MoveCommand,\
    IdleCommand, UseItemCommand
from game.utils.helper import *

#######################
# Things that can act #
#######################
class AgentAI:
    @staticmethod
    def decide(state, prev_state=None) -> Command:
        return IdleCommand()


class SimpleAI(AgentAI):
    unit_type = DEFAULT_UNIT

    @staticmethod
    def decide(state, prev_state=None) -> Command:
        agent_state = state['self']

        # Agent parameters
        print("Agent parameters:")
        print("Agent attack:", agent_state["atk"])
        print("Agent health:", agent_state["health"])
        print("Agent hunger:", agent_state["hunger"])
        print("Agent movement speed:", agent_state["movement"])

        # Case: Agent can pick up item(i.e standing point item location)
        if state['takeables']:
            random.shuffle(state['takeables'])
            return TakeItemCommand(state['takeables'][0])

        # Case: Agent has items in inventory to use
        if state['usables']:
            usables = sorted(state['usables'], key=lambda x : x['hunger_gain'], reverse=True)
            usables = sorted(state['usables'], key=lambda x : x['health_gain'], reverse=True)
            return UseItemCommand(usables[0])
        
        # Case: There are targets for agent to attack
        if state['attackables']:
            attackables = sorted(state['attackables'], key=lambda x : x['health'])
            
            player_attackables = get_player_targets(attackables)
            monster_attackables = get_monster_attackables(attackables)

            return AttackCommand(attackables[0])

        # Case: Agent moves towards items
        if state['indirect_takeables']:
            weapons = filter_weapons(state['indirect_takeables'])
            equipment = filter_equipment(state['indirect_takeables'])
            usables = filter_usables(state['indirect_takeables'])

            if weapons:
                nearest_weapon = get_nearest_item(weapons)
                directions = get_movement_directions(
                    nearest_weapon['shortest_path'], agent_state['movement']
                )

            if equipment:
                nearest_equipment = get_nearest_item(equipment)
                directions = get_movement_directions(
                    nearest_equipment['shortest_path'], agent_state['movement']
                )

            if usables:
                nearest_usable = get_nearest_item(usables)
                directions = get_movement_directions(
                    nearest_usable['shortest_path'], agent_state['movement']
                )

            return MoveCommand(directions)

        # Case: Agent moves towards attackable targets
        if state['indirect_attackables']:
            nearest_target = get_nearest_target(state['indirect_attackables'])

            directions = get_movement_directions(
                nearest_target['shortest_path'], agent_state['movement']
            )

            return MoveCommand(directions)
            
        return IdleCommand()


class MonsterSimpleAI(AgentAI):
    @staticmethod
    def decide(state, prev_state=None):
         # Attack nearest lowest health player
        attackables = sorted(state['attackables'], key=lambda x: x['health'])
        for attackable in attackables:
            if 'player_id' in attackable:
                return AttackCommand(attackable)

        # Sort indirect_attackables by nearness and then lowest HP for ties
        indirect_attackables = sorted(state['indirect_attackables'],
                                    key=lambda x: (len(x['shortest_path']), x['health']),
                                    reverse=False)

        for agent in indirect_attackables:
            shortest_path = agent['shortest_path']
            if 'player_id' in agent and len(shortest_path) <= 2 and random.random() < .80:
                print(f'Chasing {agent["name"]}')
                steps = min(
                    state['self']['movement'],  len(shortest_path) - 1
                )
                first_move_dir = shortest_path[:steps]
                return MoveCommand(first_move_dir)

        random.shuffle(state['movables'])
        for movable in state['movables']:
            return MoveCommand(movable)

        return IdleCommand()


class PlayerOneAI(SimpleAI):
    @staticmethod
    def decide(state, prev_state=None):
        ##################
        # Your Code here #
        ##################

        ##################
        # Your Code here #
        ##################
        return SimpleAI.decide(state)


class PlayerTwoAI(SimpleAI):
    @staticmethod
    def decide(state, prev_state=None):
        return SimpleAI.decide(state)


class PlayerThreeAI(SimpleAI):
    @staticmethod
    def decide(state, prev_state=None):
        return SimpleAI.decide(state)


class PlayerFourAI(SimpleAI):
    @staticmethod
    def decide(state, prev_state=None):
        return SimpleAI.decide(state)


PLAYER_AIS = [PlayerOneAI, PlayerTwoAI, PlayerThreeAI, PlayerFourAI]
